<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="el">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/5.0.0/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>ageit.gr</title>
  </head>
  <body>
  	<div class="jumbotron">
  		<h1>ageit.gr</h1>
	</div>
 	 <div class="container">
 	 <div class="row">
 	 	<p>
 	 		Καθώς βρισκόμαστε σε φάση ανάπτυξης, οι συγκεκριμένες σελίδες είναι διαθέσιμες μόνο για τους διαχειριστές του συστήματος.
 	 	</p>
 	 </div>
    	<form action="Login" method="post">
			<div class="form-group">
				<label for="formGroupExampleInput">Username</label>
				<input type="text" class="form-control" name="username" id="formGroupExampleInput" placeholder="Username">
			</div>
			<div class="form-group">
				<label for="formGroupExampleInput2">Password</label>
				<input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-primary">Σύνδεση στην εφαρμογή</button>
		</form>


		<hr />
		<p>
			To site θα φιλοξενηθεί στο <a href="https://www.vpscheap.net/">https://www.vpscheap.net/</a>
		</p>

 	</div>
    
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="jquery/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script src="bootstrap/5.0.0/js/bootstrap.bundle.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>