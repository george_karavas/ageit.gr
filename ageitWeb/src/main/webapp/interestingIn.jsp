<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<%@include file="header.jsp" %>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<a class="navbar-brand" href="index.jsp">ageit.gr</a>
  	</nav>
	<div class="container">
		<div class="row">
			<h1>Εκδήλωση ενδιαφέροντος</h1>
		</div>
		
		<div class="row">
			<p>
				Βρήκατε κάτι ενδιαφέρον στις σελίδες μας; Θέλετε να επικοινωνίσουμε μαζί σας; Συμπληρώστε την ακόλουθη φόρμα και θα το κάνουμε.
			</p>
		</div>
		

	    	<form action="Login" method="post">
				<div class="form-group">
					<label for="email">e-mail επικοινωνίας</label>
					<input type="text" class="form-control" name="email" id="email" placeholder="user@example.gr">
				</div>

				<div class="form-group">
					<label for="phone">Τηλέφωνο επικοινωνίας</label>
					<input type="text" class="form-control" name="phone" id="phone" placeholder="+30 2101234567">
				</div>

				<div class="form-group">
					<label for="comments">Σχόλια</label>
					<textarea class="form-control" id="comments" rows="5"></textarea>
				</div>

				<button type="submit" class="btn btn-primary">Αποστολή</button>
			</form>			

		
	</div>

<%@include file="footer.jsp" %>

</body>
</html>