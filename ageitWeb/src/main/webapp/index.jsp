<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="el">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- link rel="stylesheet" href="bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">

    <title>ageit.gr</title>
  </head>
  <body>
  	<div class="bg-light">
  		<div class="container-fluid">
	  		<h1>ageit.gr</h1>
	  		<p>
	  			Η ageit.gr είναι μια νέα ομάδα επαγγελματιών του χώρου της πληροφορικής, με πολυετή εμπειρία σε μεγάλα έργα. 
	  		</p>
	  		<p><a class="btn btn-primary btn-lg" href="login.jsp" role="button">Περιοχή μελών &raquo;</a></p>
  		</div>
	</div>
 	 <div class="container">
 	 	<nav class="nav justify-content-end">
 	 		<a class="nav-link" href="#whoWeAre">Ποιοί είμαστε</a>
 	 		<a class="nav-link" href="#services">Υπηρεσίες</a>
 	 		<a class="nav-link" href="#projects">Έργα</a>
 	 		<a class="nav-link" href="#clients">Πελάτες</a>
 	 		<a class="nav-link" href="#contactUs">Επικοινωνία</a>
 	 	</nav>
		<br />
		<div class="row">
			<div class="col-md-8">
				<div id="whoWeAre">
					<h4>Ποιοι είμαστε</h4>
					<p>
						Η ageit.gr είναι μια ομάδα επαγγελματιών του χώρου της πληροφορικής, με πολυετή εμπειρία σε μεγάλα έργα.
					</p>
					<p>
						Η έναρξη των δραστηριοτήτων μας αναμένεται να γίνει μέσα στο 2021. Μέχρι τότε, απλώς προετοιμαζόμαστε.
					</p>
				</div>
				<div id="services">
					<h4>Υπηρεσίες</h4>
					<p>
						Η εταιρεία μας θα μπορεί να προσφέρει λύσεις στον χώρο της πληροφορικής, έχοντας ως κύρια ενασχολήση της την παροχή outsourcing υπηρεσιών.
					</p>
					<p>
						Εξειδίκευσή μας αποτελεί η παροχή υπηρεσιών ανάπτυξης για το backend των εφαρμογών με τη διαχείριση των δεδομένων της εφαρμογής με περιφερειακά σύστηματα καθώς και με τη διασύνδεσή της με άλλες εφαρμογές.
					</p>
					<p>
						Επίσης θα αναλαμβάνει την ανάπτυξη ολοκληρωμένων εφαρμογών για τους πελάτες της.
					</p>
				</div>
				<div id="projects">
					<h4>Έργα</h4>
					<p>
						Δεδομένου ότι η εταιρεία δεν έχει ξεκινήσει ακόμη τις δραστηριοτητές της, η παρούσα ενότητα είναι κενή.
					</p>
					<p>
						Πάντως, θα δημιουργηθεί ένα προϊόν που θα είναι στη διάθεση του καθένα (open-source) ως βιτρίνα των υπηρεσιών μας. Περισσότερα για αυτό το προϊόν, μπορείτε να δείτε <a href="product1.jsp">εδώ</a>.
					</p>
				</div>
				<div id="clients">
					<h4>Πελάτες</h4>
					<p>
						Για να υπάρχουν πελάτες, πρέπει να υπάρχει και εταιρεία. Προς το παρόν είμαστε σε φάση προετοιμασίας.
					</p>
					<p>
						Αν δραστηριοποιήστε στο χώρο της εκπαίδευσης, και ενδιαφέρεστε να συμμετάσχετε στην ανάπτυξη του προϊόντος μας, θα μπορούσατε να δείτε <a href="product1.jsp">εδώ</a> περισσότερες λεπτομέρειες.
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<p><a class="btn btn-success btn-lg" href="interestingIn.jsp" role="button">Εκδήλωση ενδιαφέροντος &raquo;</a></p>
			</div>
		</div>
		
    </div>
    
		<div id="contactUs" class="container-fluid">
			<hr />
			&copy;<a href="https://ageit.gr">ageit.gr</a> 2021
			<p>
				+30 6946339704
				<br><a href="mailto:support@gkaravas.com">support@gkaravas.com</a>
			</p>
		</div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="jquery/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <!-- script src="bootstrap/4.1.3/js/bootstrap.bundle.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script -->
    <script src="bootstrap/js/bootstrap.bundle.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
  </body>
</html>