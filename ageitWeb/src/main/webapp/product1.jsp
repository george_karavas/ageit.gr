<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<%@include file="header.jsp" %>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<a class="navbar-brand" href="index.jsp">ageit.gr</a>
  	</nav>
	<div class="container">
		<div class="row">
			<h1>Product1</h1>
		</div>
		
		<div class="row">
			<p>
				Το πρώτο μας ολοκληρωμένο προϊόν θα αποτελεί ένα προϊόν που θα αφορά τη διαχείρηση εκπαιδευτικών οργανισμών.
			</p>
			<p>
				Θα είναι ελεύθερο λογισμικό (open-source) και θα φιλοξενείται σε κάποιον από τους γνωστούς servers.
			</p>
			<p>
				Περισσότερες λεπτομέρειες θα γίνουν γνωστές κατά την ανάπτυξή του.
			</p>
		</div>
	</div>

<%@include file="footer.jsp" %>
</body>
</html>