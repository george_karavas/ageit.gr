/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor.project;

import gr.ageit.web.constructor.Constants;
import gr.ageit.web.constructor.ageit.pages.AgeItIndex;

/**
 *
 * @author georgekaravas
 */
public class AgeItProject extends GkProject{

    public AgeItProject(String buildFor, String projectName) {
        super(Constants.BOOTSTRAP, "ageit.gr");
    }

    @Override
    public void setup() {
        AgeItIndex index = new AgeItIndex(this.getBuildFor());
        index.setFilename("index.php");
        getPages().add(index);
    }
    
}
