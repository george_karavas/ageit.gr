/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor.project;

import gr.ageit.web.constructor.GkHtmlPage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author georgekaravas
 */
public abstract class GkProject {
    private String buildFor;
    private String projectName;
    protected List<GkHtmlPage> pages;

    public GkProject(String buildFor, String projectName) {
        this.buildFor = buildFor;
        this.projectName = projectName;
        this.pages = new ArrayList<GkHtmlPage>();
    }

    public String getBuildFor() {
        return buildFor;
    }

    public String getProjectName() {
        return projectName;
    }

    public List<GkHtmlPage> getPages() {
        return pages;
    }
    
    public abstract void setup();
    
}
