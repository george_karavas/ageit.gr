/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

/**
 *
 * @author georgekaravas
 */
public abstract class GkHtmlStaticText extends GkHtmlElement{
    private String text;
    
    public GkHtmlStaticText(String buildFor) {
        super(buildFor);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    
    @Override
    protected String getBootstrapElementString() {
        return this.text;
    }

}
