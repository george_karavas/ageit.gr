/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author georgekaravas
 */
public abstract class GkHtmlElement {
    private final String    buildFor;
    private List<GkHtmlElement> elements;

    public GkHtmlElement(String buildFor) {
        this.buildFor = buildFor;
    }

    public String getBuildFor() {
        return buildFor;
    }

    public void addElement(GkHtmlElement element){
        if( elements == null ){
            elements = new ArrayList<GkHtmlElement>();
        }
        elements.add(element);
    }

    public List<GkHtmlElement> getElements() {
        return elements;
    }
    
    protected abstract String getBootstrapElementString();
    
    public String getElementString(){
        String ans = "";
        if( this.buildFor.equalsIgnoreCase(Constants.BOOTSTRAP) ){
            ans = getBootstrapElementString();
        }
        return ans;
    }
}
