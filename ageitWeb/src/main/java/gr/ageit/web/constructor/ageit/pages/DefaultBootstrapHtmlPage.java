/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor.ageit.pages;

import gr.ageit.web.constructor.GkHtmlBody;
import gr.ageit.web.constructor.GkHtmlHead;
import gr.ageit.web.constructor.GkHtmlPage;
import gr.ageit.web.constructor.GkHtmlStaticText;

/**
 *
 * @author georgekaravas
 */
public abstract class DefaultBootstrapHtmlPage extends GkHtmlPage{
    private GkHtmlStaticText defaultBody;
    
    public DefaultBootstrapHtmlPage(String buildFor) {
        super(buildFor);
        setHead( new GkHtmlHead(buildFor){});
        setBody( new GkHtmlBody(buildFor){});
    }

    @Override
    public void setup() {
        GkHtmlStaticText defaultHead = new GkHtmlStaticText(this.getBuildFor()) {};
        defaultHead.setText("" +
            "    <!-- Required meta tags -->\n" +
            "    <meta charset=\"utf-8\">\n" +
            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n" +
            "\n" +
            "    <!-- Bootstrap CSS -->\n" +
            "    <link rel=\"stylesheet\" href=\"bootstrap/css/bootstrap.min.css\" integrity=\"sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I\" crossorigin=\"anonymous\">\n" +
            "");
        this.getHead().addElement(defaultHead);
        this.getHead().setTitle("ageit.gr");

        defaultBody = new GkHtmlStaticText(this.getBuildFor()) {};
        defaultBody.setText("" +
            "    <!-- Optional JavaScript -->\n" +
            "    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\n" +
            "    <script src=\"jquery/jquery-3.5.1.slim.min.js\" integrity=\"sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=\" crossorigin=\"anonymous\"></script>\n" +
            "    <script src=\"bootstrap/js/bootstrap.bundle.js\" integrity=\"sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/\" crossorigin=\"anonymous\"></script>\n" +
            "");
    }

    @Override
    protected String getBootstrapElementString() {
        getBody().addElement(this.defaultBody);
        return super.getBootstrapElementString();
    }
    
    
}
