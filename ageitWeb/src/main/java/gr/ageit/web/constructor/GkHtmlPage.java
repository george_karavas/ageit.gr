/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

/**
 *
 * @author georgekaravas
 */
public abstract class GkHtmlPage extends GkHtmlElement{
    private String lang;
    private GkHtmlHead head;
    private GkHtmlBody body;
    private String filename;

    public GkHtmlPage(String buildFor) {
        super(buildFor);
        lang = "el";
    }

     public void setHead(GkHtmlHead head) {
        this.head = head;
    }

    public void setBody(GkHtmlBody body) {
        this.body = body;
    }

    public GkHtmlHead getHead() {
        return head;
    }

    public GkHtmlBody getBody() {
        return body;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    public abstract void setup();

    
    @Override
    protected String getBootstrapElementString() {
        String ans = "<!doctype html>\n" 
                +"<html";
        if(this.lang!=null){
            ans = ans+" lang=\""+this.lang+"\"";
        }
        ans = ans+">\n";
        ans = ans.concat(this.getHead().getElementString());
        ans = ans.concat(this.getBody().getElementString());
        ans = ans.concat("\n</html>\n");
        return ans;
    }
    
}
