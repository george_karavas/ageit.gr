/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

/**
 *
 * @author georgekaravas
 */
public abstract class GkHtmlBody extends GkHtmlElement{
    
    public GkHtmlBody(String buildFor) {
        super(buildFor);
    }

    @Override
    protected String getBootstrapElementString() {
        String ans = "<body>\n";
        if(this.getElements()!=null){
            for( GkHtmlElement element : this.getElements()){
                ans = ans+element.getBootstrapElementString()+"\n";
            }
        }
        return ans+"</body>\n";
    }

}
