/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor.ageit.pages;

import gr.ageit.web.constructor.GkDiv;
import gr.ageit.web.constructor.GkHtmlStaticText;

/**
 *
 * @author georgekaravas
 */
public class AgeItIndex extends DefaultBootstrapHtmlPage{
    
    public AgeItIndex(String buildFor) {
        super(buildFor);
    }

    @Override
    public void setup() {
        super.setup();
        setFilename("index.php");
        GkDiv topDiv = new GkDiv(this.getBuildFor()) {};
        topDiv.setClassString("bg-light");
        
        GkDiv jumbotron = new GkDiv(this.getBuildFor()) {};
        jumbotron.setClassString("container-fluid");
        
        GkHtmlStaticText title = new GkHtmlStaticText(this.getBuildFor()) {};
        title.setText("" +
                "	  		<h1>ageit.gr</h1>\n" +
                "	  		<p>\n" +
                "	  			Η ageit.gr είναι μια νέα ομάδα επαγγελματιών του χώρου της πληροφορικής, με πολυετή εμπειρία σε μεγάλα έργα. \n" +
                "	  		</p>\n" +
                "");
        
        jumbotron.addElement(title);
        topDiv.addElement(jumbotron);
        getBody().addElement(topDiv);
    }
    
}
