/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

/**
 *
 * @author georgekaravas
 */
public abstract class GkDiv extends GkHtmlElement{
    private String classString;
    
    public GkDiv(String buildFor) {
        super(buildFor);
    }

    public String getClassString() {
        return classString;
    }

    public void setClassString(String classString) {
        this.classString = classString;
    }

    @Override
    protected String getBootstrapElementString() {
        String ans = "<div";
        if(this.classString!=null){
            ans = ans+" class=\""+classString+"\"";
        }
        ans = ans+">\n";
        if(this.getElements()!=null){
            for( GkHtmlElement element : this.getElements()){
                ans = ans+element.getBootstrapElementString()+"\n";
            }
        }
        return ans+"</div>\n";
    }

}
