/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.constructor;

/**
 *
 * @author georgekaravas
 */
public abstract class GkHtmlHead extends GkHtmlElement{
    private String title;

    public GkHtmlHead(String buildFor) {
        super(buildFor);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    protected String getBootstrapElementString() {
        String ans = "<head>\n";
        if(this.getElements()!=null){
            for( GkHtmlElement element : this.getElements()){
                ans = ans+element.getBootstrapElementString()+"\n";
            }
        }
        if(this.getTitle()!=null){
            ans = ans+"   <title>"+this.getTitle()+"</title>\n";
        }
        return ans+"</head>\n";
    }
    
}
