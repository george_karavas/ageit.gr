/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.ageit.web.servlet;

import gr.ageit.web.constructor.Constants;
import gr.ageit.web.constructor.GkHtmlPage;
import gr.ageit.web.constructor.project.GkProject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author georgekaravas
 */
@WebServlet(name = "ShowProject", urlPatterns = {"/ShowProject"})
public class ShowProject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
//            String projectClass = "gr.ageit.web.constructor.project.AgeItProject";
//            String buildFor = Constants.BOOTSTRAP;
//            String projectName = "ageit.gr";
            String projectClass = request.getParameter("projectClass");
            String buildFor = request.getParameter("buildFor");
            String projectName = request.getParameter("projectName");
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowProject</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowProject at " + request.getContextPath() + "</h1>");
            out.println("<h3>" + projectName + "</h3>");
            out.println("<h3>" + buildFor + "</h3>");
            out.println("<h3>" + projectClass + "</h3>");
            Class<?> clazz = Class.forName(projectClass); //new AgeItProject(Constants.BOOTSTRAP, "ageit.gr");
            Constructor<?> constructor = clazz.getConstructor(String.class, String.class);
            Object object = constructor.newInstance(buildFor,projectName);
            GkProject project = (GkProject)object;
            project.setup();
            for(GkHtmlPage page : project.getPages()){
                out.println("<p><a href=\"ShowPage?buildFor="+page.getBuildFor()+"&projectName="+projectName+"&pageClass="+page.getClass().getName()+"\">"+page.getFilename()+"</a></p>");
            }
            out.println("</body>");
            out.println("</html>");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(ShowProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
